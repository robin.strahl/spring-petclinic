package org.springframework.samples.petclinic.exceptions;

public class CustomRuntimeException extends Exception{
    public CustomRuntimeException(String errorMessage){
        super(errorMessage);
    }
}
