# SonarLint Report for main **before** fixing issues:
14 issues in 7 files.

## CacheConfiguration.java
### [1] Minor:
- Remove useless curly braces around statement
### [0] Major:
### [0] Critical:

## CrashController.java
### [0] Minor:
### [1] Major:
- Define and throw a dedicated exception instead of using a generic one.
### [0] Critical:

## OwnerController.java
### [0] Minor:
### [0] Major:
### [3] Critical:
- [3] Replace this persistent entity with a simple POJO or DTO object.

## PetController.java
### [0] Minor:
### [0] Major:
### [5] Critical:
- [5] Replace this persistent entity with a simple POJO or DTO object.

## VetController.java
### [0] Minor:
### [2] Major:
- [2] Rename "vets" which hides the field declared at line 33.
### [0] Critical:

## Vets.java
### [0] Minor:
### [1] Major:
- Rename field "vets"
### [0] Critical:

## VisitController.java
### [0] Minor:
### [0] Major:
### [1] Critical:
- Replace this persistent entity with a simple POJO or DTO object.

# SonarLint Report for main **after** fixing issues:
9 issues in 3 files.

## CacheConfiguration.java
### [0] Minor:
### [0] Major:
### [0] Critical:

## CrashController.java
### [0] Minor:
### [0] Major:
### [0] Critical:

## OwnerController.java
### [0] Minor:
### [0] Major:
### [3] Critical:
- [3] Replace this persistent entity with a simple POJO or DTO object.

## PetController.java
### [0] Minor:
### [0] Major:
### [5] Critical:
- [5] Replace this persistent entity with a simple POJO or DTO object.

## VetController.java
### [0] Minor:
### [0] Major:
### [0] Critical:

## Vets.java
### [0] Minor:
### [0] Major:
### [0] Critical:

## VisitController.java
### [0] Minor:
### [0] Major:
### [1] Critical:
- Replace this persistent entity with a simple POJO or DTO object.

*Note: couldn´t get the OwnerInDTO running.*
